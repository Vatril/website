from flask import Flask, render_template
import json
import os

app = Flask(__name__)
app.config['TEMPLATES'] = "templates"


@app.route('/')
@app.route('/<page>')
def index(page="home"):
    if page not in ["home", "projects", "imprint", "contact", "sona"]:
        return render_template('404.html'), 404
    if page == "sona":
        with open(os.path.dirname(os.path.realpath(__file__)) + "/sona.json") as sona:
            return render_template('{0}.html'.format(page), images=json.load(sona)), 200
    return render_template('{0}.html'.format(page)), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
